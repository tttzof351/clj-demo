(defproject clj-demo "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [http-kit "2.2.0"] 
		 [org.clojure/data.xml "0.0.8"]	
		 [org.clojure/data.zip "0.1.2"] 
		 [com.cemerick/url "0.1.1"] 
		 [compojure "1.5.2"] 
		 [javax.servlet/servlet-api "2.5"] 
		 [cheshire "5.7.0"] 
		 [http-kit.fake "0.2.1"]]
  :main ^:skip-aot clj-demo.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  :plugins [[lein-cljfmt "0.5.6"]])
