(ns clj-demo.core-test
  (:require [clojure.test :refer :all]
            [clj-demo.core :refer :all]))

(use '[org.httpkit.fake])

(deftest parse-link-test
  (testing "parse-link"
    (is (= (parse-link "http://lenta.ru/photo/2012/11/28/lis/") "lenta.ru"))
    (is (= (parse-link "https://music.yandex.ru/search?query=Boby") "yandex.ru"))))

(def fake-yandex-response
  "<?xml version=\"1.0\" encoding=\"utf-8\" ?>
		<rss>
			<channel>
				<item>
					<link>http://vk.com/wall123</link>
				</item>
			</channel>
		</rss>")

(deftest fake-test
  (testing "fake test"
    (is (= (with-fake-http [(ya-link "scala") fake-yandex-response] (search-blogs ["scala"]))
           {"vk.com" 1}))))
