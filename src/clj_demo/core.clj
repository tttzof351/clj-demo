(ns clj-demo.core
  (:gen-class))

(require '[org.httpkit.client :as http]
         '[org.httpkit.server :as server]
         '[clojure.data.xml :as dxml]
         '[clojure.data.zip.xml :as zxml]
         '[clojure.zip :as zip]
         '[cemerick.url :refer (url url-encode)]
         '[org.httpkit.timer :as timer]
         '[cheshire.core :as json])

(use '[compojure.route :only [not-found]]
     '[compojure.handler :only [site]]
     '[compojure.core :only [defroutes GET context]])

(def client-pool (java.util.concurrent.Executors/newFixedThreadPool 2))

(defn to-xml [rss] (zip/xml-zip (dxml/parse-str rss)))

(defn parse-rss [xrss] (take 10 (zxml/xml-> xrss :rss :channel :item :link zxml/text)))

(defn parse-link [link] (-> (:host (url link))
                            (#(clojure.string/split % #"\."))
                            (#(take-last 2 %))
                            (#(clojure.string/join "." %))))

(defn ya-link [query] (str "http://yandex.ru/blogs/rss/search?text=" query))

(defn parse-result [result] (-> result
                                :body
                                to-xml
                                parse-rss))

(defn to-seq [elem] (if (instance? String elem) [elem] elem))

(defn parse-params [req] (to-seq (get (:query-params req) "query")))

(defn search-blogs [queries] (let [promises (doall (map #(http/get % {:worker-pool client-pool}) (map ya-link queries)))
                                   results  (doall (map deref promises))]
                               (-> results
                                   (#(map parse-result %))
                                   (#(reduce concat %))
                                   distinct
                                   (#(map parse-link %))
                                   frequencies)))

(defn get-responce [req channel] (let [queries (parse-params req)
                                       result (search-blogs queries)
                                       json-result (json/generate-string result {:pretty true})]
                                   (do
                                     (server/send! channel {:tatus 200 :headers {"Content-Type" "application/json"} :body json-result})
                                     (server/close channel))))

(defn async-handle [req]
  (server/with-channel req channel (get-responce req channel)))

(defroutes all-routes
  (GET "/search" [] async-handle)
  (not-found "<p>Page not found.</p>"))

(defn -main
  [& args]
  (server/run-server (site #'all-routes) {:port 8080}))
